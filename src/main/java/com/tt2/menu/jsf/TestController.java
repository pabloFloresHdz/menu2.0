/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tt2.menu.jsf;

import com.tt2.menu.entidades.Articulo;
import com.tt2.menu.entidades.Categoria;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Pablo
 */
@Named(value = "testController")
@RequestScoped
public class TestController {

    @EJB 
    private com.tt2.menu.ejbs.ArticuloDAO dao;
    private ArrayList<Articulo>articulos;
    private ArrayList<Categoria>cats;
    public TestController() {
    }
    public ArrayList<Articulo>getArticulos(){
        if(articulos==null){
            articulos=dao.getAll();
            System.out.println("Controller:"+articulos.size());
        }
        return articulos;
    }
    public ArrayList<Categoria>getCategorias(){
        if(cats==null)
            cats=dao.getCategorias(1);
        return cats;
    }
    private String id="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }  
    private ArrayList<Articulo>articulosPorCategoria;
    public ArrayList<Articulo>getArticulosPorCategoria(String idCategoria){
        return getArticulos();
    }
}
