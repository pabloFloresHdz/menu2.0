/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tt2.menu.ejbs;

import com.tt2.menu.entidades.Articulo;
import com.tt2.menu.entidades.Categoria;
import java.util.ArrayList;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

/**
 *
 * @author Pablo
 */
@Stateful
public class ArticuloDAO {
    @PersistenceContext(unitName = "MenuPU",type = PersistenceContextType.EXTENDED)
    private EntityManager em;
    public ArrayList<Articulo>getAll(){
        Query q = em.createQuery("Select a from Articulo a");
        ArrayList<Articulo>lista=new ArrayList<>(q.getResultList());
        return lista;
    }
    public ArrayList<Categoria>getCategorias(int id_menu){
        Query q = em.createQuery("Select a.categoria from ArticuloMenuCategoria a WHERE a.menu.idMenu=:id_menu GROUP BY a.categoria.idCategoria")
                .setParameter("id_menu", id_menu);
        ArrayList<Categoria>lista=new ArrayList<>(q.getResultList());
        return lista;
    }
}
