<div class=" navbar navbar-default navbar-fixed-top" role="navigation">
    <div class=" container">
        <div class=" navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class=" sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Restaurante MX</a>
        </div>
        <div class=" collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.jsp">Inicio</a></li>
                <li><a href="verMenu.jsp">Men� del Restaurante</a></li>
                <li><a href="verOrden.jsp">Orden Act�al</a></li>
                <li><a href="verComprobante.jsp">Comprobante</a></li>
                <li><a href="encuesta.jsp">Encuesta de Satisfacci�n</a></li>
                <li><a href="acercade.jsp">Acerca de Nosotros</a></li>
            </ul>
        </div>
    </div>
</div>