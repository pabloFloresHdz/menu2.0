-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.5-10.0.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla tt2.articulo
CREATE TABLE IF NOT EXISTS `articulo` (
  `id_articulo` int(10) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(20) DEFAULT 'PRUEBARFC123',
  `nombre` varchar(100) DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `descripcion` text,
  `imagen` varchar(100) DEFAULT NULL,
  `tiempo_preparacion` time DEFAULT '10:30:00',
  `calorias` decimal(10,0) DEFAULT '200',
  `disponible` tinyint(1) DEFAULT '1' COMMENT 'No hay booleanos, se utilizará 0 para no disponible y 1 para disponible',
  PRIMARY KEY (`id_articulo`),
  KEY `FK_articulo_restaurante` (`rfc`),
  CONSTRAINT `FK_articulo_restaurante` FOREIGN KEY (`rfc`) REFERENCES `restaurante` (`rfc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla tt2.articulo: ~74 rows (aproximadamente)
DELETE FROM `articulo`;
/*!40000 ALTER TABLE `articulo` DISABLE KEYS */;
INSERT INTO `articulo` (`id_articulo`, `rfc`, `nombre`, `precio`, `descripcion`, `imagen`, `tiempo_preparacion`, `calorias`, `disponible`) VALUES
	(1, 'PRUEBARFC123', 'Sopa Especial', 45, 'Sopa con fresco pollo deshebrado con arroz blanco', 'sopaespecial.jpg', '10:30:00', 200, 1),
	(2, 'PRUEBARFC123', 'Sopa del Día', 45, 'Sopa de arroz blanco servida en tazón', 'sopadeldia.jpg', '10:30:00', 200, 1),
	(3, 'PRUEBARFC123', 'Sopa de Fideos', 40, 'Sopa de fideo condimentado con salsa de tomate', 'sopadefideo.jpg', '10:30:00', 200, 1),
	(4, 'PRUEBARFC123', 'Sopa de Verduras', 48, 'Sopa de zanahoria, col y elote', 'sopadeverduras.jpg', '10:30:00', 200, 1),
	(5, 'PRUEBARFC123', 'Sopa de tortilla', 40, 'Sopa de tortilla con arroz blanco', 'sopadetortilla.jpg', '10:30:00', 200, 1),
	(6, 'PRUEBARFC123', 'Consome de pollo', 45, 'Tradicional consome de pollo con garbanzo', 'consome.jpg', '10:30:00', 200, 1),
	(7, 'PRUEBARFC123', 'Ensalada especial', 75, 'Ensalada de pollo con tiras de jamon y tocino', 'ensaladaespecial.jpg', '10:30:00', 200, 1),
	(8, 'PRUEBARFC123', 'Ensalada cesar', 65, 'Clasica ensalada cesar', 'ensaladacesar.jpg', '10:30:00', 200, 1),
	(9, 'PRUEBARFC123', 'Ensalada con pollo', 60, 'Ensalada con tiras de pollo y aderezo al gusto', 'ensaladapollo.jpg', '10:30:00', 200, 1),
	(10, 'PRUEBARFC123', 'Ensalada baja en calorías', 65, 'Ensalada de lechuga, espinacas, jitomate y vinagreta', 'ensaladabaja.jpg', '10:30:00', 200, 1),
	(11, 'PRUEBARFC123', 'Torre de jitomate', 75, 'Ensalada con queso panela, aguacate, vinagret y cebollín', 'torrejitomate.jpg', '10:30:00', 200, 1),
	(12, 'PRUEBARFC123', 'Ensalada de espinacas', 60, 'Ensalada con champiñones, jitomate, germen de frijo y vinagret', 'ensaladaespinacas.jpg', '10:30:00', 200, 1),
	(13, 'PRUEBARFC123', 'Ensalada de pollo o atún', 60, 'Ensalada servida sobre hoja de lechuga con mayonesa, apio, huevo cocido y jitomate', 'ensaladapollo.jpg', '10:30:00', 200, 1),
	(14, 'PRUEBARFC123', 'Fettuccini Paja y Heno', 90, 'con salsa bolognesa y queso parmesano', 'fettuccini.jpg', '10:30:00', 200, 1),
	(15, 'PRUEBARFC123', 'Spaghetti al Estilo Alfredo', 88, 'con salsa crema, queso parmesano y perejil picado', 'spaghetti.jpg', '10:30:00', 200, 1),
	(16, 'PRUEBARFC123', 'Canelones Rellenos con Espinacas', 92, 'jamon endiablado y salsa italiana', 'canelones.jpg', '10:30:00', 200, 1),
	(17, 'PRUEBARFC123', 'Brocheta de Camarones', 80, 'a la plancha con pimiento morrón, cebolla y mojoo de ajo, sobr arroz blanco', 'brocheta.jpg', '10:30:00', 200, 1),
	(18, 'PRUEBARFC123', 'Tacos de Camarones Estilo Culiacán (3)', 75, 'con queso chihuahua, cebolla, chile jalapeño y salsa roja o verde', 'tacoscamaron.jpg', '10:30:00', 200, 1),
	(19, 'PRUEBARFC123', 'Filete de Pescado', 80, 'al mojo de ajo con arroz blanco o empanizado con puré de papa', 'filetepescado.jpg', '10:30:00', 200, 1),
	(20, 'PRUEBARFC123', 'Filete de Pescado Empanizado Crujiente', 70, 'con ensalada rusa', 'pescadoempanizado.jpg', '10:30:00', 200, 1),
	(21, 'PRUEBARFC123', 'Camarones Empanizados', 105, 'a la francesa, con salsa tártara o a la plancha, sobre arroz', 'camaronesempanizados.jpg', '10:30:00', 200, 1),
	(22, 'PRUEBARFC123', 'Sabana de Res Gratinada', 115, 'con champiñones al ajillo, jamon queso y guacamole', 'sabanares.jpg', '10:30:00', 200, 1),
	(23, 'PRUEBARFC123', 'Sabanita de Res Gratinada en 2 salsas', 110, ' verde y roja al cilantro con enchiladas', 'sabanitares.jpg', '10:30:00', 200, 1),
	(24, 'PRUEBARFC123', 'Filete Mignon', 90, 'con papas a la francesa,  lechuga y jitomate o con salsa de champiñones', 'filetemignon.jpg', '10:30:00', 200, 1),
	(25, 'PRUEBARFC123', 'Carne Asada', 100, 'delicioso corte de filete de res, con taco de queso, rajas poblanas y frijoles refritos', 'carneasada.jpg', '10:30:00', 200, 1),
	(26, 'PRUEBARFC123', 'Arrachera', 125, 'servida con papas a la francesa y frescas rebanadas de jitomate', 'arrachera.jpg', '10:30:00', 200, 1),
	(27, 'PRUEBARFC123', 'Milanesa Empanizada', 100, 'con papas a la francesa, lechuga romanita, jitomate y cebolla', 'milanesa.jpg', '10:30:00', 200, 1),
	(28, 'PRUEBARFC123', 'Fajitas de Pollo o Arrachera', 125, 'con cebolla, tocino, pimiento morrón rojo y verde', 'fajitas.jpg', '10:30:00', 200, 1),
	(29, 'PRUEBARFC123', 'Puntas de Pollo', 100, 'a los 3 chiles con frijoles refritos', 'puntaspollo.jpg', '10:30:00', 200, 1),
	(30, 'PRUEBARFC123', 'Medio Pollo a la Parrilla', 180, 'marinado en especias con papas fritas, cebolla, oregano y salsa molcajeteada', 'mediopollo.jpg', '10:30:00', 200, 1),
	(31, 'PRUEBARFC123', 'Suprema de Pollo', 160, 'rellena de queso gruyere, con salsa cassé de tomate y rissoto a la mantequilla', 'suprema.jpg', '10:30:00', 200, 1),
	(32, 'PRUEBARFC123', 'Pechuga a la Poblana', 145, 'con arroz a la mexicana o a la parrilla, con verduras', 'pechugapoblana.jpg', '10:30:00', 200, 1),
	(33, 'PRUEBARFC123', 'Chilaquiles', 75, 'con queso al gratín, crema, cebolla y frijoles refritos', 'chilaquiles.jpg', '10:30:00', 200, 1),
	(34, 'PRUEBARFC123', 'Pepitos de Filete o Arrachera', 85, 'con papas a la francesa, cebolla, chile chipotle y ajo', 'pepitos.jpg', '10:30:00', 200, 1),
	(35, 'PRUEBARFC123', 'Tacos de Cochinita Pibil (4)', 89, 'con cebolla morada en escabeche, salsa pibil, frijoles refritos, queso y totopos', 'tacoscochinita.jpg', '10:30:00', 200, 1),
	(36, 'PRUEBARFC123', 'Chile Relleno de Queso Mozzarella', 68, 'en salsa pasilla, crema, frijoles refritos, queso y totopos', 'chilerelleno.jpg', '10:30:00', 200, 1),
	(37, 'PRUEBARFC123', 'Tacos de Pollo (4)', 70, 'con guacamole, crema, frijoles refritos, queso y totopos', 'tacospollo.jpg', '10:30:00', 200, 1),
	(38, 'PRUEBARFC123', 'Tacos de Arrachera (3)', 90, 'a la parrilla con guacamole verde y cebollitas', 'tacosarrachera.jpg', '10:30:00', 200, 1),
	(39, 'PRUEBARFC123', 'Molletes (4)', 80, 'bolillos con frijoles, gratinados con queso', 'molletes.jpg', '10:30:00', 200, 1),
	(40, 'PRUEBARFC123', 'Tecolotes (4)', 70, 'bolillos con mantequilla, chilaquiles con queso gratinado y frijoles refritos', 'tecolotes.jpg', '10:30:00', 200, 1),
	(41, 'PRUEBARFC123', 'Burritos Norteños (4)', 70, 'de jamon, milanesa o cochinita pibil con salsa molcajeteada', 'burritos.jpg', '10:30:00', 200, 1),
	(42, 'PRUEBARFC123', 'Pays y Pasteles Surtidos', 45, 'Gran variedad de pasteles de tres leches y pays helados', 'pasteles.jpg', '10:30:00', 200, 1),
	(43, 'PRUEBARFC123', 'Pastel de Queso con Fresas', 40, 'cubierto con crema batida y glassé', 'pastelqueso.jpg', '10:30:00', 200, 1),
	(44, 'PRUEBARFC123', 'Rollo Helado', 35, 'bañado con chocolate caliente, crema batida y nueces', 'rollohelado.jpg', '10:30:00', 200, 1),
	(45, 'PRUEBARFC123', 'Pay Helado de Limon', 45, 'Pay helado de limon o limon light', 'paylimon.jpg', '10:30:00', 200, 1),
	(46, 'PRUEBARFC123', 'Gelatina de Sabores o Flan', 30, 'Gelatina servida en copa o flan acompañado de rompope', 'gelatinas.jpg', '10:30:00', 200, 1),
	(47, 'PRUEBARFC123', 'Nieve', 25, 'Ricas nieves estilo poblano', 'nieves.jpg', '10:30:00', 200, 1),
	(48, 'PRUEBARFC123', 'Hot Fudge de Chocolate', 50, 'helado de vainilla, bañado con chocolate caliente, crema batida y trocitos de nuez', 'hotfudge.jpg', '10:30:00', 200, 1),
	(49, 'PRUEBARFC123', 'Tres Marias', 40, 'helados de fresa, chocolate y vainilla, bañados con jarabes de los mismos sabores, crema batida y trocitos de nuez', 'tresmarias.jpg', '10:30:00', 200, 1),
	(50, 'PRUEBARFC123', 'Banana Split', 50, 'plátano rebanado a mitades, con helados de fresa, chocolate y vainilla, acompañados con mermelada de fresa y crema batida', 'bananasplit.jpg', '10:30:00', 200, 1),
	(51, 'PRUEBARFC123', 'Malteadas', 35, 'chocolate, vainilla o fresa, servidas con crema batida y galletas', 'malteada.jpg', '10:30:00', 200, 1),
	(52, 'PRUEBARFC123', 'Limonada o Naranjada', 25, '-', 'limonada.jpg', '10:30:00', 200, 1),
	(53, 'PRUEBARFC123', 'Refresco Grande', 22, 'Variedad de Sabores de la familia Coca-Cola', 'refrescos.jpg', '10:30:00', 200, 1),
	(54, 'PRUEBARFC123', 'Refresco Light en Lata', 25, 'Variedad de Sabores de la familia Coca-Cola', 'refrescoslight.jpg', '10:30:00', 200, 1),
	(55, 'PRUEBARFC123', 'Chocolate caliente o frío', 28, 'Delicioso chocolate batido servido en jarra', 'chocolate.jpg', '10:30:00', 200, 1),
	(56, 'PRUEBARFC123', 'Té Caliente o Frío', 15, 'con crema o  limón', 'te.jpg', '10:30:00', 200, 1),
	(57, 'PRUEBARFC123', 'Leche', 15, '-', 'leche.jpg', '10:30:00', 200, 1),
	(58, 'PRUEBARFC123', 'Agua Embotellada', 20, 'Agua Ciel de medio litro', 'agua.jpg', '10:30:00', 200, 1),
	(59, 'PRUEBARFC123', 'Cervezas', 32, 'Variedad de Cervezas Mexicanas (Indio, León, Corona)', 'cervezas.jpg', '10:30:00', 200, 1),
	(60, 'PRUEBARFC123', 'Café', 30, 'Varidad de café (Express, Instantáneo, con Leche y Cappuccino)', 'cafe.jpg', '10:30:00', 200, 1),
	(61, 'PRUEBARFC123', 'Dos Huevos', 60, 'tibios o poché', 'huevos.jpg', '10:30:00', 200, 1),
	(62, 'PRUEBARFC123', 'Huevos con Chorizo', 55, '-', 'huevoschorizo.jpg', '10:30:00', 200, 1),
	(63, 'PRUEBARFC123', 'Huevos al Gusto', 70, 'rancheros, a la mexicana o al albañil', 'huevosgusto.jpg', '10:30:00', 200, 1),
	(64, 'PRUEBARFC123', 'Omelet de Queso o Champiñones', 60, '-', 'omelet.jpg', '10:30:00', 200, 1),
	(65, 'PRUEBARFC123', 'Huevos Revueltos con Huarache', 60, 'con salsa verde y queso cotija', 'huevosrevueltos.jpg', '10:30:00', 200, 1),
	(66, 'PRUEBARFC123', 'Huevos Motuleños', 60, 'con salsa motul y plátanos machos', 'huevosmotulenos.jpg', '10:30:00', 200, 1),
	(67, 'PRUEBARFC123', 'Claras de Huevo', 60, 'con jocoque y zathar de jamaica', 'clarashuevo.jpg', '10:30:00', 200, 1),
	(68, 'PRUEBARFC123', 'Caldo de pollo', 45, 'Tradicional caldo de pollo con verduras y cebolla', 'caldodepollo.jpg', '10:30:00', 200, 1),
	(69, 'PRUEBARFC123', 'Caldo de Camaron', 45, 'Caldo de camaron con trozos de pescado fresco', 'caldodecamaron.jpg', '10:30:00', 200, 1),
	(70, 'PRUEBARFC123', 'Sopa de Cebolla con Ajo', 45, 'Sopa de Cebolla con Ajo', 'sopadecebolla.jpg', '10:30:00', 200, 1),
	(71, 'PRUEBARFC123', 'Sopa de concha con aguacate y brócoli', 95, 'Una rica sopa de pasta con ingredientes saludables', 'sopadeconcha.jpg', '10:30:00', 200, 1),
	(72, 'PRUEBARFC123', 'Albondiguitas y tortellini', 93, 'Deliciosas albóndigas con pasta tortellini', 'tortellini.jpg', '10:30:00', 200, 1),
	(73, 'PRUEBARFC123', 'Sopa de Calabaza con Papa', 40, 'Tradicional sopa servida con trozos de ajo y cebolla', 'sopadecalabaza.jpg', '10:30:00', 200, 1),
	(74, 'PRUEBARFC123', 'Sopa Italiana', 55, 'Sopa hecha a base de distintas pastas servida con salsa de tomate y perejil', 'sopaitaliana.jpg', '10:30:00', 200, 1);
/*!40000 ALTER TABLE `articulo` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.articulo_menu_categoria
CREATE TABLE IF NOT EXISTS `articulo_menu_categoria` (
  `id_articulo` int(10) NOT NULL DEFAULT '0',
  `id_menu` int(10) NOT NULL DEFAULT '0',
  `id_categoria` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_articulo`,`id_menu`,`id_categoria`),
  KEY `FK_articulo_menu_categoria_menu` (`id_menu`),
  KEY `FK_articulo_menu_categoria_categoria` (`id_categoria`),
  CONSTRAINT `FK_articulo_menu_categoria_articulo` FOREIGN KEY (`id_articulo`) REFERENCES `articulo` (`id_articulo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_articulo_menu_categoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_articulo_menu_categoria_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla tt2.articulo_menu_categoria: ~17 rows (aproximadamente)
DELETE FROM `articulo_menu_categoria`;
/*!40000 ALTER TABLE `articulo_menu_categoria` DISABLE KEYS */;
INSERT INTO `articulo_menu_categoria` (`id_articulo`, `id_menu`, `id_categoria`) VALUES
	(1, 1, 1),
	(2, 1, 1),
	(3, 1, 1),
	(4, 1, 1),
	(5, 1, 1),
	(6, 1, 1),
	(7, 1, 2),
	(8, 1, 2),
	(9, 1, 2),
	(10, 1, 2),
	(11, 1, 2),
	(12, 1, 2),
	(13, 1, 2),
	(70, 1, 1),
	(71, 1, 1),
	(73, 1, 1),
	(74, 1, 1);
/*!40000 ALTER TABLE `articulo_menu_categoria` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.articulo_orden
CREATE TABLE IF NOT EXISTS `articulo_orden` (
  `no_orden` int(10) NOT NULL,
  `id_menu` int(10) DEFAULT '0',
  `id_categoria` int(10) DEFAULT '0',
  `id_articulo` int(10) DEFAULT '0',
  `cantidad` int(10) DEFAULT '0',
  `extras` text,
  PRIMARY KEY (`no_orden`),
  KEY `FK__articulo_menu_categoria_4` (`id_menu`,`id_categoria`,`id_articulo`),
  CONSTRAINT `FK__articulo_menu_categoria_4` FOREIGN KEY (`id_menu`, `id_categoria`, `id_articulo`) REFERENCES `articulo_menu_categoria` (`id_articulo`, `id_menu`, `id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla tt2.articulo_orden: ~0 rows (aproximadamente)
DELETE FROM `articulo_orden`;
/*!40000 ALTER TABLE `articulo_orden` DISABLE KEYS */;
/*!40000 ALTER TABLE `articulo_orden` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(10) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(20) DEFAULT 'PRUEBARFC123',
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_categoria`),
  KEY `FK_categoria_restaurante` (`rfc`),
  CONSTRAINT `FK_categoria_restaurante` FOREIGN KEY (`rfc`) REFERENCES `restaurante` (`rfc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla tt2.categoria: ~9 rows (aproximadamente)
DELETE FROM `categoria`;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`id_categoria`, `rfc`, `nombre`, `descripcion`) VALUES
	(1, 'PRUEBARFC123', 'Sopas', 'Variedad de sopas ideales para iniciar la comida'),
	(2, 'PRUEBARFC123', 'Ensaladas', 'Frescas ensaladas para complementar o suplir una comida.'),
	(3, 'PRUEBARFC123', 'Pastas', 'Deliciosas pastas preparadas con recetas italianas.'),
	(4, 'PRUEBARFC123', 'Mariscos y Pescados', 'Frescos productos marinos de la mas alta calidad.'),
	(5, 'PRUEBARFC123', 'Carnes y Aves', 'Variedad de cortes sobre carnes y diversos tipos de aves preparadas.'),
	(6, 'PRUEBARFC123', 'Platillos Mexicanos', 'Exquisitos y tradicionales platillos nacionales.'),
	(7, 'PRUEBARFC123', 'Postres', 'Postres ideales para terminar una comida.'),
	(8, 'PRUEBARFC123', 'Bebidas', 'Refrescantes bebidas para acompañar los alimentos.'),
	(9, 'PRUEBARFC123', 'Desayunos', 'Platillos ideales para iniciar el dia con energía.');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.datosencuesta
CREATE TABLE IF NOT EXISTS `datosencuesta` (
  `no_encuesta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `recomendacion` int(10) unsigned NOT NULL DEFAULT '0',
  `satisfaccion` int(10) unsigned NOT NULL DEFAULT '0',
  `calidad` int(10) unsigned NOT NULL DEFAULT '0',
  `prontitud` int(10) unsigned NOT NULL DEFAULT '0',
  `limpieza` int(10) unsigned NOT NULL DEFAULT '0',
  `ambiente` int(10) unsigned NOT NULL DEFAULT '0',
  `valor` int(10) unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `rfc` varchar(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`no_encuesta`),
  KEY `FK_DatosEncuesta_restaurante` (`rfc`),
  CONSTRAINT `FK_DatosEncuesta_restaurante` FOREIGN KEY (`rfc`) REFERENCES `restaurante` (`rfc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Datos de la encuesta del servicio general del restaurante';

-- Volcando datos para la tabla tt2.datosencuesta: ~0 rows (aproximadamente)
DELETE FROM `datosencuesta`;
/*!40000 ALTER TABLE `datosencuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `datosencuesta` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.empleado
CREATE TABLE IF NOT EXISTS `empleado` (
  `nss` int(10) NOT NULL,
  `rfc` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `passwd` varchar(50) NOT NULL,
  PRIMARY KEY (`nss`),
  KEY `FK_Empleado_restaurante` (`rfc`),
  CONSTRAINT `FK_Empleado_restaurante` FOREIGN KEY (`rfc`) REFERENCES `restaurante` (`rfc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla que se encarga de resgistrar a todos los empleados de un establecimiento';

-- Volcando datos para la tabla tt2.empleado: ~0 rows (aproximadamente)
DELETE FROM `empleado`;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id_menu` int(10) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(20) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `descripcion` text,
  `disponible` tinyint(1) DEFAULT '1' COMMENT 'No hay booleanos, 0 para no disponible, 1 para disponible ',
  PRIMARY KEY (`id_menu`),
  KEY `FK_menu_restaurante` (`rfc`),
  CONSTRAINT `FK_menu_restaurante` FOREIGN KEY (`rfc`) REFERENCES `restaurante` (`rfc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla tt2.menu: ~0 rows (aproximadamente)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id_menu`, `rfc`, `nombre`, `fecha_inicio`, `fecha_fin`, `descripcion`, `disponible`) VALUES
	(1, 'PRUEBARFC123', 'Menú Actual', '2014-08-04', '2014-08-04', 'Menú Actual', 1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Volcando estructura para tabla tt2.restaurante
CREATE TABLE IF NOT EXISTS `restaurante` (
  `rfc` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `logotipo` varchar(100) NOT NULL,
  `mapa_ubicacion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `horario` varchar(50) NOT NULL,
  `detalles_extra` text NOT NULL,
  PRIMARY KEY (`rfc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla que contiene las características de un restaurante';

-- Volcando datos para la tabla tt2.restaurante: ~0 rows (aproximadamente)
DELETE FROM `restaurante`;
/*!40000 ALTER TABLE `restaurante` DISABLE KEYS */;
INSERT INTO `restaurante` (`rfc`, `nombre`, `direccion`, `logotipo`, `mapa_ubicacion`, `telefono`, `horario`, `detalles_extra`) VALUES
	('PRUEBARFC123', 'Restaurante MX', 'Nose', 'logo.jpg', 'ubicacion', '12345678', '10 am a 10pm', 'Prueba detalles');
/*!40000 ALTER TABLE `restaurante` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
